package schoolbell;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 * This class deals with the basic operations on database This class uses
 * Schooldatabase of root@localhost
 *
 * @author dhaka
 *
 */
public class SchoolDao {

	private static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	private static final String DB_URL = "jdbc:mysql://localhost/schooldatabase";
	private static final String USER = "root";
	private static final String PASS = "phoenix@1990";
	private Connection connection = null;

	public SchoolDao() {

	}

	/**
	 * 
	 * @return connection returns connection instance
	 */
	Connection createConnection() {
		try {
			Class.forName(JDBC_DRIVER).newInstance();
		} catch (InstantiationException e) {

			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {

			e.printStackTrace();
		}
		try {
			connection = (Connection) DriverManager.getConnection(DB_URL, USER,
					PASS);
		} catch (SQLException e) {

			e.printStackTrace();
		}
		return connection;
	}

	/**
	 *
	 * @param sql
	 *            the query string
	 * @param statement
	 *            the respective statement object bound to query
	 * @return return the result of query
	 * 
	 */
	ResultSet getMessage(String sql, Statement statement) {
		ResultSet resultset = null;
		this.createConnection();
		try {
			resultset = statement.executeQuery(sql);

			return resultset;
		} catch (SQLException e) {

			e.printStackTrace();
			return resultset;
		}

	}

	/**
	 * 
	 * @param sql
	 *            the query string
	 * @param statement
	 *            the respective statement object bound to query
	 * @throws SQLException
	 * 
	 */
	ResultSet changeStatus(String s1, PreparedStatement statement) {
		ResultSet resultSet = null;
		try {
			statement.executeQuery();
			resultSet = statement.getResultSet();

		} catch (SQLException e) {

			e.printStackTrace();
		}

		return resultSet;
	}

	
	void closeConnection() {
		try {
			connection.close();
		} catch (SQLException e) {

			e.printStackTrace();
		}
	}

}
