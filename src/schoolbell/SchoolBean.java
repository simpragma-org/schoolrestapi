package schoolbell;

/**
 * This is a containor class for school objects
 * @author Sunil Dhaka
 *
 */
public class SchoolBean {

	private String schoolName;
	private String address;
	private int pin;
	public SchoolBean() {
		
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public int getPin() {
		return pin;
	}
	public void setPin(int pin) {
		this.pin = pin;
	}

}
