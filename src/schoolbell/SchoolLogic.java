package schoolbell;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.google.gson.Gson;

/**
 * This is the query formation and result retrieving class
 * @author Sunil Dhaka
 *
 */
public class SchoolLogic {

	/**
	 * 
	 * @return Json String
	 */
	public String responseOperationAllSchools() {
		SchoolDao schoolDataObject = new SchoolDao();
		Connection connection = null;
		String s1 = "select * from schools";
		connection = schoolDataObject.createConnection();
		Statement statement = null;
		try {
			statement = (Statement) connection.createStatement();
		} catch (SQLException e1) {

			e1.printStackTrace();
		}

		ResultSet resultset = null;
		resultset = schoolDataObject.getMessage(s1, statement);
		ArrayList<SchoolBean> al1 = new ArrayList<SchoolBean>();
		try {
			while (resultset.next()) {
				SchoolBean schoolBean = new SchoolBean();
				schoolBean.setSchoolName(resultset.getString("schoolname"));
				schoolBean.setAddress(resultset.getString("address"));
				schoolBean.setPin(resultset.getInt("pin"));
				al1.add(schoolBean);

			}
		} catch (SQLException e) {
			
			e.printStackTrace();
		}
		Gson gson = new Gson();
		String json = gson.toJson(al1);
		schoolDataObject.closeConnection();
		return json;
	}

	
	/**
	 * 
	 * @param pin
	 * @return
	 */
	public String responseOperationPinBased(int pin) {

		SchoolDao schoolDataObject = new SchoolDao();
		Connection connection = null;
		ResultSet resultset = null;
		java.sql.PreparedStatement statement = null;
		String s1 = "select * from schools where pin=?";
		connection = schoolDataObject.createConnection();
		ArrayList<SchoolBean> al1 = new ArrayList<SchoolBean>();
		String json = null;

		try {
			statement = connection.prepareStatement(s1);
			statement.setInt(1, pin);
			resultset = schoolDataObject.changeStatus(s1, statement);
			while (resultset.next()) {
				SchoolBean schoolBean = new SchoolBean();
				schoolBean.setSchoolName(resultset.getString("schoolname"));
				schoolBean.setAddress(resultset.getString("address"));
				schoolBean.setPin(resultset.getInt("pin"));
				al1.add(schoolBean);
				System.out.println(resultset.getString("schoolname"));
			}
			Gson gson = new Gson();
			json = gson.toJson(al1);

		} catch (SQLException e1) {

			e1.printStackTrace();
		}
		schoolDataObject.closeConnection();
		return json;
	}
}
