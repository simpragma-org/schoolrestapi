package schoolbell;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
/**
 * This class provides a simple school data access service 
 * @author Sunil Dhaka
 *
 */
@Path("/schools")
public class SchoolApi {

	
/**
 * 	
 * @param pincode
 * @return json string
 */
	

	  
	  @GET
	  @Path(value = "")
	  @Produces(MediaType.APPLICATION_JSON)
	  
	  public String listOfSchoolsPinBased(@QueryParam("pincode") int pincode) {
		  if(pincode>0)
		  {
		  	    return (new SchoolLogic().responseOperationPinBased(pincode));
		  }
		  else if(pincode<0)
			  return "NOT A PIN CODE";
			  else
			  return (new SchoolLogic().responseOperationAllSchools());
	  }

	} 
